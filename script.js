"use strict";

let changeKeyColor = function () {
    const element = document.querySelectorAll('.btn');

    document.addEventListener("keydown", function (event) {
        element.forEach(elem => {
            let keyLetter = event.key.slice(0, 1).toUpperCase() + event.key.slice(1);
            if (elem.textContent === keyLetter) {
                changeToBlackColor();
                elem.style.backgroundColor = "blue";
            };
        });

        function changeToBlackColor() {
            element.forEach(elem => {
                elem.style.backgroundColor = "black";
            });
        };

    });
};

changeKeyColor();